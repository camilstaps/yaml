implementation module Text.YAML.Represent

/**
 * Copyright 2021 Camil Staps.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv
import StdGeneric
import StdMaybe

import Control.Applicative
import Control.Monad
import Data.Error
import Data.Func
import Data.Functor
import Data.List

import Text.YAML
import Text.YAML.Compose
import Text.YAML.Schemas

representAsYAML :: !YAMLSchema !a -> MaybeError YAMLError YAMLNode | gRepresentAsYAML{|*|} a
representAsYAML schema x = gRepresentAsYAML{|*|} schema False x >>= \nodes -> case nodes of
	[node] -> Ok node
	[] -> Error (InvalidContent "no result from gRepresentAsYAML{|*|}")
	_ -> Error (InvalidContent "too many results from gRepresentAsYAML{|*|}")

withDefaultProperties content :==
	{ properties = {tag = "?", anchor = ?None}
	, content = content
	}

generic gRepresentAsYAML a :: !YAMLSchema !Bool !a -> MaybeError YAMLError [YAMLNode]

gRepresentAsYAML{|UNIT|} _ _ _ = Ok []

gRepresentAsYAML{|EITHER|} fl _ schema in_record (LEFT l) = fl schema in_record l
gRepresentAsYAML{|EITHER|} _ fr schema in_record (RIGHT r) = fr schema in_record r

gRepresentAsYAML{|PAIR|} fx fy schema in_record (PAIR x y) = liftA2 (++) (fx schema in_record x) (fy schema in_record y)

gRepresentAsYAML{|OBJECT|} fx schema _ (OBJECT x) = fx schema False x

gRepresentAsYAML{|CONS of {gcd_name,gcd_arity}|} fx schema in_record (CONS x)
	| gcd_arity == 0 =
		Ok [key]
	| otherwise =
		toMapping <$> fx schema in_record x
where
	key = withDefaultProperties $ YAMLScalar Plain gcd_name

	toMapping [arg] = pure $ withDefaultProperties $ YAMLMapping
		[{key=key, value=arg}]
	toMapping args = pure $ withDefaultProperties $ YAMLMapping
		[{key=key, value=withDefaultProperties (YAMLSequence args)}]

gRepresentAsYAML{|RECORD of {grd_fields}|} fx schema _ (RECORD x) = toMapping <$> fx schema True x
where
	toMapping nodes = pure $ withDefaultProperties $ YAMLMapping
		[{key=field f, value=n} \\ f <- grd_fields & n <- nodes]
	where
		field f =
			{ properties = {tag = "!", anchor = ?None}
			, content = YAMLScalar SingleQuoted f
			}

gRepresentAsYAML{|FIELD|} fx schema in_record (FIELD x) = fx schema in_record x

gRepresentAsYAML{|String|} schema in_record x =
	representScalar TAG_STR schema in_record (YAMLScalar SingleQuoted x)
gRepresentAsYAML{|Int|} schema in_record x =
	representScalar TAG_INT schema in_record (YAMLScalar Plain (toString x))
gRepresentAsYAML{|Real|} schema in_record x =
	representScalar TAG_FLOAT schema in_record (YAMLScalar Plain (toString x))
gRepresentAsYAML{|Char|} schema in_record x =
	representScalar TAG_STR schema in_record (YAMLScalar SingleQuoted {#x})

representScalar tag schema in_record content = case resolveTag schema node of
	Ok resolved | resolved == tag ->
		Ok [node]
	_ ->
		Ok [{node & properties.tag=tag}]
where
	node =
		{ properties = {tag = "?", anchor = ?None}
		, content = content
		}

gRepresentAsYAML{|[]|} fx schema in_record xs =
	pure o withDefaultProperties o YAMLSequence o flatten <$> mapM (fx schema in_record) xs

gRepresentAsYAML{|[!]|}  fx schema in_record xs = gRepresentAsYAML{|*->*|} fx schema in_record [x \\ x <|- xs]
gRepresentAsYAML{|[ !]|} fx schema in_record xs = gRepresentAsYAML{|*->*|} fx schema in_record [x \\ x <|- xs]
gRepresentAsYAML{|[!!]|} fx schema in_record xs = gRepresentAsYAML{|*->*|} fx schema in_record [x \\ x <|- xs]
gRepresentAsYAML{|[#]|}  fx schema in_record xs = gRepresentAsYAML{|*->*|} fx schema in_record [x \\ x <|- xs]
gRepresentAsYAML{|[#!]|} fx schema in_record xs = gRepresentAsYAML{|*->*|} fx schema in_record [x \\ x <|- xs]

gRepresentAsYAML{|{}|}  fx schema in_record xs = gRepresentAsYAML{|*->*|} fx schema in_record [x \\ x <-: xs]
gRepresentAsYAML{|{!}|} fx schema in_record xs = gRepresentAsYAML{|*->*|} fx schema in_record [x \\ x <-: xs]

gRepresentAsYAML{|(?)|} fx schema False ?None = Ok [withDefaultProperties (YAMLScalar Plain "")]
gRepresentAsYAML{|(?)|} fx schema False (?Just x) = pure o withDefaultProperties o YAMLSequence <$> fx schema False x
gRepresentAsYAML{|(?)|} fx schema True ?None = Ok [withDefaultProperties (YAMLScalar Plain "")]
gRepresentAsYAML{|(?)|} fx schema True (?Just x) = fx schema False x

gRepresentAsYAML{|(?^)|} fx schema in_record mbX = gRepresentAsYAML{|*->*|} fx schema in_record $ convertMaybe mbX
gRepresentAsYAML{|(?#)|} fx schema in_record mbX = gRepresentAsYAML{|*->*|} fx schema in_record $ convertMaybe mbX

convertMaybe m :== case m of
	?|None -> ?None
	?|Just x -> ?Just x

gRepresentAsYAML{|()|} _ _ _ = Ok [withDefaultProperties (YAMLSequence [])]

gRepresentAsYAML{|(,)|} fa fb schema in_record (a,b) =
	pure o withDefaultProperties o YAMLSequence o flatten <$> sequence
		[ fa schema in_record a
		, fb schema in_record b
		]

gRepresentAsYAML{|(,,)|} fa fb fc schema in_record (a,b,c) =
	pure o withDefaultProperties o YAMLSequence o flatten <$> sequence
		[ fa schema in_record a
		, fb schema in_record b
		, fc schema in_record c
		]

gRepresentAsYAML{|(,,,)|} fa fb fc fd schema in_record (a,b,c,d) =
	pure o withDefaultProperties o YAMLSequence o flatten <$> sequence
		[ fa schema in_record a
		, fb schema in_record b
		, fc schema in_record c
		, fd schema in_record d
		]

gRepresentAsYAML{|(,,,,)|} fa fb fc fd fe schema in_record (a,b,c,d,e) =
	pure o withDefaultProperties o YAMLSequence o flatten <$> sequence
		[ fa schema in_record a
		, fb schema in_record b
		, fc schema in_record c
		, fd schema in_record d
		, fe schema in_record e
		]

gRepresentAsYAML{|(,,,,,)|} fa fb fc fd fe ff schema in_record (a,b,c,d,e,f) =
	pure o withDefaultProperties o YAMLSequence o flatten <$> sequence
		[ fa schema in_record a
		, fb schema in_record b
		, fc schema in_record c
		, fd schema in_record d
		, fe schema in_record e
		, ff schema in_record f
		]

gRepresentAsYAML{|(,,,,,,)|} fa fb fc fd fe ff fg schema in_record (a,b,c,d,e,f,g) =
	pure o withDefaultProperties o YAMLSequence o flatten <$> sequence
		[ fa schema in_record a
		, fb schema in_record b
		, fc schema in_record c
		, fd schema in_record d
		, fe schema in_record e
		, ff schema in_record f
		, fg schema in_record g
		]

gRepresentAsYAML{|(,,,,,,,)|} fa fb fc fd fe ff fg fh schema in_record (a,b,c,d,e,f,g,h) =
	pure o withDefaultProperties o YAMLSequence o flatten <$> sequence
		[ fa schema in_record a
		, fb schema in_record b
		, fc schema in_record c
		, fd schema in_record d
		, fe schema in_record e
		, ff schema in_record f
		, fg schema in_record g
		, fh schema in_record h
		]
