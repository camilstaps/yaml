implementation module Text.YAML.JSON

/**
 * Copyright 2021 Camil Staps.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Control.Applicative
import Control.Monad
import Control.Monad.State
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import Data.Tuple
from Text import class Text(toLowerCase), instance Text String
import Text.GenJSON

import Text.YAML
import Text.YAML.Compose
import Text.YAML.Schemas

:: ToJSONState =
	{ anchored_nodes :: [(String, JSONNode)] // cannot be a Map; this would cause a cycle in spine
	}

error :: !YAMLError -> StateT s (MaybeError YAMLError) a
error s = StateT \_ -> Error s

yamlToJSON :: !YAMLSchema !YAMLNode -> MaybeError YAMLError JSONNode
yamlToJSON schema node = evalStateT (toJSON node) initState
where
	initState =
		{ anchored_nodes = []
		}

	// We do not use Text.YAML.Contruct's removeAliases because then we would
	// have to convert aliased nodes twice. Instead, we link the anchor
	// directly to the converted JSON.
	toJSON node=:{properties={anchor = ?Just anchor}} = StateT anchor_and_parse
	where
		anchor_and_parse st =
			let
				// NB: potential cycle in spine
				mbJsonAndSt = runStateT
					(toJSON {node & properties.anchor = ?None})
					{st & anchored_nodes = [(anchor, json):st.anchored_nodes]}
				(json,st`) = fromOk mbJsonAndSt
			in
			mbJsonAndSt
	toJSON node=:{properties={tag},content=YAMLScalar _ s} = case resolveTag schema node of
		Error e -> error e
		Ok tag -> scalarToJSON tag s
	toJSON {content=YAMLSequence xs} = JSONArray <$> mapM toJSON xs
	toJSON {content=YAMLMapping xs} = JSONObject <$> mapM convertKeyValue xs
	where
		convertKeyValue {key,value} = liftA2 tuple (keyToJSON key) (toJSON value)

		keyToJSON node = toJSON node >>= \json -> case json of
			JSONString s -> pure s
			_ -> error (InvalidContent "YAMLMapping contains complex key")
	toJSON {content=YAMLAlias alias} = StateT fetch_alias
	where
		fetch_alias st = case lookup alias st.anchored_nodes of
			?Just json -> pure (json, st)
			?None -> Error (UnidentifiedAlias alias)

	scalarToJSON tag s
		| tag == TAG_STR
			= pure $ JSONString s
		| tag == TAG_INT
			= pure $ JSONInt $ schema.toInt s
		| tag == TAG_NULL
			= pure JSONNull
		| tag == TAG_BOOL
			= pure $ JSONBool (s=="true" || s=="True" || s=="TRUE")
		| tag == TAG_FLOAT
			= pure $ JSONReal $ schema.toReal s
			= pure $ JSONString s
