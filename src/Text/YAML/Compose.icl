implementation module Text.YAML.Compose

/**
 * Copyright 2021 Camil Staps.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Control.Applicative
import Control.Monad
import Data.Error
import Data.Functor
import Data.Maybe

import Text.YAML
import Text.YAML.Parse
import Text.YAML.ParseUtils

instance == YAMLNode
where
	(==) x y = x.properties == y.properties && x.content == y.content

instance == YAMLNodeContent
where
	(==) (YAMLScalar t1 c1) (YAMLScalar t2 c2) = t1 == t2 && c1 == c2
	(==) (YAMLScalar _ _) _ = False
	(==) (YAMLSequence xs) (YAMLSequence ys) = xs == ys
	(==) (YAMLSequence _) _ = False
	(==) (YAMLMapping xs) (YAMLMapping ys) = xs == ys
	(==) (YAMLMapping _) _ = False
	(==) (YAMLAlias x) (YAMLAlias y) = x == y
	(==) (YAMLAlias _) _ = False

instance == YAMLScalarType
where
	(==) Plain x = x=:Plain
	(==) SingleQuoted x = x=:SingleQuoted
	(==) DoubleQuoted x = x=:DoubleQuoted
	(==) Literal x = x=:Literal
	(==) Folded x = x=:Folded

instance == YAMLKeyValuePair
where
	(==) p1 p2 = p1.key == p2.key && p1.value == p2.value

instance == YAMLNodeProperties
where
	(==) p1 p2 = p1.tag == p2.tag && p1.anchor == p2.anchor

node = liftA2 (\p c -> {properties=properties p c, content=c})
	(pFastOptional node_properties)
	node_content
where
	properties (?Just p) _ = p
	properties _ (YAMLScalar Plain _) = {tag = "?", anchor = ?None}
	properties _ (YAMLScalar _ _) = {tag = "!", anchor = ?None}
	properties _ _ = {tag = "?", anchor = ?None}

node_properties =
	(\(PropertiesEvent ps) -> ps) <$> pSatisfy (\t -> t=:(PropertiesEvent _))

node_content
	=    scalar
	<<|> sequence
	<<|> mapping
	<<|> alias

scalar =
	(\(ScalarEvent t s) -> YAMLScalar t s) <$> pSatisfy (\t -> t=:(ScalarEvent _ _))

sequence =
	pToken EnterSequence >>|
	(YAMLSequence <$> pMany node) <*
	pToken ExitSequence

mapping =
	pToken EnterMapping >>|
	(YAMLMapping <$> pMany key_value_pair) <*
	pToken ExitMapping
key_value_pair = liftA2 (\k v -> {key=k, value=v}) node node

alias =
	(\(AliasEvent a) -> YAMLAlias a) <$> pSatisfy (\t -> t=:(AliasEvent _))

document =
	pSatisfy (\t -> t=:(EnterDocument _)) >>|
	node <*
	pSatisfy (\t -> t=:(ExitDocument _))
documents = pMany document

composeYAMLGraphs :: ![YAMLEvent] -> MaybeError YAMLError [YAMLNode]
composeYAMLGraphs events = fst <$> parse documents () {!e \\ e <- events}
