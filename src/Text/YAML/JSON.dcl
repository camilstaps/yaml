definition module Text.YAML.JSON

/**
 * This module defines a function to convert YAML to JSON.
 *
 * Copyright 2021 Camil Staps.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.Error import :: MaybeError
from Text.GenJSON import :: JSONNode

from Text.YAML import :: YAMLError
from Text.YAML.Compose import :: YAMLNode
from Text.YAML.Schemas import :: YAMLSchema

yamlToJSON :: !YAMLSchema !YAMLNode -> MaybeError YAMLError JSONNode
