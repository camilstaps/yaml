# YAML

A [YAML][] processor for [Clean][].

## Author &amp; License
This library is maintained by [Camil Staps][].

Copyright is owned by the authors of individual commits, including:

- Camil Staps
- Gijs Alberts

This project is licensed under GPL v3; see the [LICENSE](/LICENSE) file.

We use the [YAML Test Suite][].

[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
[YAML]: https://yaml.org
[YAML Test Suite]: https://github.com/yaml/yaml-test-suite
